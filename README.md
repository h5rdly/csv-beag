# csv-beag

Making CSV parsing great again

## Usage

```cpp
# include "big_csv.cpp"
# include <iostream>


int main () {
    
    // Load a CSV from path. Column names and types supplied as a map
    csv_meta_map csv_meta = {{"col1", col_types.Long}, {"col2", col_types.String}, {"col3", col_types.Int}};
    auto csv = CSV("path/to/sample.csv", csv_meta);
    
    // Parse desired amount of rows by supplying view of csv and the columns
    [[maybe_unused]] auto rows_parsed = csv.parse_all();
    
    // Grab desired items, parsed data stored in vector columns
    [[maybe_unused]] long long_col_item = csv[0][1];
    cout << "\n check long: " << long_col_item <<  '\n';  
}
```